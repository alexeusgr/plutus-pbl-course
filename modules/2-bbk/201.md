---
aliases: "Three ways to mint a token"
type: "BBK"
learning-targets: [
	"I can mint a Cardano native asset in GameChanger",
	"I can mint a Mary-era native asset using cardano-cli",
	"I can mint a native asset using Plutus",
]
lead-teacher: "James"
go-live-date: 2022-07-25
---

[[ppbl-curriculum-outline]]

## Outcomes / Projects #outcomes 
1. You've got tokens, maybe a few of them!
2. You can distinguish between when each is useful

## Notes
1. Here we start to apply Plutus. Starting to introduce Redeemer and Context
2. Should we talk about UTxO model vs. Account-Based Model in this module? We can then compare ways to build tokens
