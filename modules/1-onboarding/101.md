---
aliases: "Prepare a Plutus Development Environment"
type: "Onboarding"
learning-targets: [
	"I can load nix shell.",
	"I can compile a simple Plutus validator script.",
]
lead-teacher: "Pedro Lucas and MIxAxIM"
go-live-date: 2022-07-18
---

[[ppbl-curriculum-outline]]

## Outcomes / Projects #outcomes 
1. Set up Plutus development environment
2. Compile a script

## Notes
1. You will need help, we'll be there for you!
2. Ok, you've got a Plutus Script...but what can you do with it? In the next module, we'll show you. First, you'll need to get `cardano-cli`, and while you're at it, you'll sync your own `cardano-node`.


Can we improve our documentation of which Plutus checkout is relevant to x, y, z?


## Further Reading / Optional Extensions:
1. What can I do with Nix?
2. (establishing connections for later on: in this course / in other courses)